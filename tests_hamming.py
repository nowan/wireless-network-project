import numpy as np

def hamming748_decode(bitSeq):
    H = np.array([
        [0, 0, 0, 1, 1, 1, 1],
        [0, 1, 1, 0, 0, 1, 1],
        [1, 0, 1, 0, 1, 0, 1]
    ])

    corrected = []

    for i in range(0, len(bitSeq), 8):
        partition = bitSeq[i:i+8]
        E = np.matmul(H, bitSeq[i:i+7])
        E = np.mod(E, 2)
        error_idx = E[0]*4 + E[1]*2 + E[2]
        
        if error_idx != 0:
            error_idx -= 1
            print(f"Error detected (idx={error_idx})")
            partition = partition
            if partition[error_idx] == 1:
                partition[error_idx] = 0
            else:
                partition[error_idx] = 1
            print(f"{partition} ===> {partition}")
        
        s = sum(bitSeq[i:i+7]) % 2
        parity = bitSeq[i+7]
        if s != parity:
            print(f"Unrecoverable problem detected (parity={parity}) (sum={s})")
        
        corrected.append(partition[0])
        corrected.append(partition[1])
        corrected.append(partition[2])
        corrected.append(partition[3])
    
    return corrected

def test_hammingDecode():
    # Decoding when no errors leads to sequence recovering
    assert np.array_equal(hamming748_decode([1, 1, 0, 1, 0, 0, 1, 0]), [1, 1, 0, 1])
    assert np.array_equal(hamming748_decode([1, 1, 0, 0, 1, 1, 0, 0]), [1, 1, 0, 0])
    assert np.array_equal(hamming748_decode([1, 1, 1, 1, 1, 1, 1, 1]), [1, 1, 1, 1])
    assert np.array_equal(hamming748_decode([0, 1, 1, 1, 1, 0, 0, 0]), [0, 1, 1, 1])
    assert np.array_equal(hamming748_decode([0, 1, 1, 0, 0, 1, 1, 0]), [0, 1, 1, 0])
    assert np.array_equal(hamming748_decode([0, 0, 1, 1, 0, 0, 1, 1]), [0, 0, 1, 1])
    assert np.array_equal(hamming748_decode([0, 0, 1, 0, 1, 1, 0, 1]), [0, 0, 1, 0])
    assert np.array_equal(hamming748_decode([0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 1]), [0, 0, 1, 1, 0, 0, 1, 0])
    # Ensure that one error is detected, and corrected
    assert np.array_equal(hamming748_decode([1, 1, 0, 1, 0, 0, 0, 0]), [1, 1, 0, 1])
    assert np.array_equal(hamming748_decode([1, 1, 0, 0, 1, 1, 0, 0]), [1, 1, 0, 0])
    assert np.array_equal(hamming748_decode([1, 0, 1, 1, 1, 1, 1, 1]), [1, 1, 1, 1])
    assert np.array_equal(hamming748_decode([0, 1, 1, 1, 1, 0, 0, 0]), [0, 1, 1, 1])
    assert np.array_equal(hamming748_decode([0, 1, 1, 0, 0, 1, 1, 0]), [0, 1, 1, 0])
    assert np.array_equal(hamming748_decode([0, 0, 1, 1, 0, 0, 1, 0]), [0, 0, 1, 1])
    assert np.array_equal(hamming748_decode([0, 0, 1, 0, 1, 1, 0, 1]), [0, 0, 1, 0])    
    # Ensure that two errors cannot be corrected
    assert not np.array_equal(hamming748_decode([1, 0, 1, 1, 0, 0, 1, 0]), [1, 1, 0, 1])
    assert not np.array_equal(hamming748_decode([1, 1, 1, 1, 1, 1, 0, 0]), [1, 1, 0, 0])
    assert not np.array_equal(hamming748_decode([0, 1, 1, 0, 1, 1, 1, 1]), [1, 1, 1, 1])
    assert not np.array_equal(hamming748_decode([1, 0, 1, 1, 1, 0, 0, 0]), [0, 1, 1, 1])
    assert not np.array_equal(hamming748_decode([1, 1, 1, 1, 0, 1, 1, 0]), [0, 1, 1, 0])
    assert not np.array_equal(hamming748_decode([0, 1, 0, 1, 0, 0, 1, 1]), [0, 0, 1, 1])
    assert not np.array_equal(hamming748_decode([0, 1, 0, 0, 1, 1, 0, 1]), [0, 0, 1, 0])

test_hammingDecode()