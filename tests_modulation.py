import numpy as np

def bpsk_demod(qamSeq: np.ndarray):
    sequence = []
    for i in qamSeq:
        sequence.append(1 if np.real(i) > 0 else 0)
    return sequence

def qpsk_demod(matrix: np.ndarray):
    demod = []
    for i in matrix:
        bit_a = 0 if np.real(i) < 0 else 1
        bit_b = 0 if np.imag(i) < 0 else 1
        demod.append(bit_a)
        demod.append(bit_b)
    return demod

def test_bpsk():
    # BPSK decoding test
    assert bpsk_demod(np.array([1.0+1j*0.0,1.0+1j*0.0,1.0+1j*0.0,-1.0+1j*0.0])) == [1, 1, 1, 0]
    assert bpsk_demod(np.array([1.0+1j*0.0,1.0+1j*0.0,-1.0+1j*0.0,1.0+1j*0.0,1.0+1j*0.0,1.0+1j*0.0,-1.0+1j*0.0,1.0+1j*0.0])) == [1, 1, 0, 1, 1, 1, 0, 1]
    assert bpsk_demod(np.array([1.0+1j*0.0,1.0+1j*0.0,1.0+1j*0.0,-1.0+1j*0.0,1.0+1j*0.0,1.0+1j*0.0,1.0+1j*0.0,-1.0+1j*0.0,-1.0+1j*0.0,-1.0+1j*0.0,1.0+1j*0.0,-1.0+1j*0.0,1.0+1j*0.0,-1.0+1j*0.0,1.0+1j*0.0,-1.0+1j*0.0])) == [1, 1, 1, 0, 1, 1, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0]
    assert bpsk_demod(np.array([-1.0+1j*0.0,-1.0+1j*0.0,-1.0+1j*0.0,-1.0+1j*0.0,-1.0+1j*0.0,-1.0+1j*0.0,1.0+1j*0.0,-1.0+1j*0.0,-1.0+1j*0.0,1.0+1j*0.0,1.0+1j*0.0,-1.0+1j*0.0,1.0+1j*0.0,-1.0+1j*0.0,-1.0+1j*0.0,-1.0+1j*0.0])) == [0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0, 0]
    assert bpsk_demod(np.array([-1.2+1j*-0.2,-0.9+1j*-0.3,-1.1+1j*0.1,-1.0+1j*-0.0,-0.8+1j*0.2,-1.1+1j*-0.0,1.0+1j*0.2,-1.0+1j*0.0,-1.0+1j*0.1,1.2+1j*0.1,1.1+1j*-0.1,-1.0+1j*-0.1,1.1+1j*-0.1,-1.0+1j*0.2,-0.8+1j*-0.1,-1.0+1j*0.1])) == [0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0, 0]

def test_qpsk():
    # QPSK decoding test
    assert qpsk_demod(np.array([-0.7+1j*-0.7,0.7+1j*-0.7])) == [0, 0, 1, 0]
    assert qpsk_demod(np.array([-0.7+1j*0.7,0.7+1j*-0.7,0.7+1j*-0.7,0.7+1j*0.7])) == [0, 1, 1, 0, 1, 0, 1, 1]
    assert qpsk_demod(np.array([-0.7+1j*0.7,0.7+1j*-0.7,0.7+1j*-0.7,-0.7+1j*-0.7,-0.7+1j*0.7,0.7+1j*-0.7,-0.7+1j*-0.7,-0.7+1j*0.7])) == [0, 1, 1, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1]
    assert qpsk_demod(np.array([-0.7+1j*0.7,-0.7+1j*-0.7,0.7+1j*-0.7,0.7+1j*0.7,-0.7+1j*0.7,0.7+1j*0.7,-0.7+1j*0.7,0.7+1j*-0.7])) == [0, 1, 0, 0, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0]
    assert qpsk_demod(np.array([-0.9+1j*0.6,-0.5+1j*-0.7,0.7+1j*-0.6,0.7+1j*0.9,-0.8+1j*0.6,0.8+1j*0.7,-0.6+1j*0.6,0.7+1j*-0.7])) == [0, 1, 0, 0, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0]

test_bpsk()
test_qpsk()
